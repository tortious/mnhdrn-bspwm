# BSPWM DOTFILES OF MNHDRN

Installation guide:
====

This setup script is mean to be use with __Opensuse Tumbleweed__ or __Opensuse Leap__.
Please choose custom install during the installation process.

You __MUST__ clone this repository in your _$HOME_, if not the script will not work.

Once the installation over, install git with this commande:
```text
sudo zypper in -y git
```
When it's done, login your new computer and run this command to download this repos:
```text
git clone https://gitlab.com/mnhdrn/mnhdrn-bspwm ~/mnhdrn-bspwm
```

then enter in the repos and just run the setup script like that:
```text
cd ~/mnhdrn-bspwm; ./setup
```

At the end you can run a script for Codecs installation, you can find it here: _$HOME/mnhdrn-bspwm/includes/codec_setup_*XXX*.sh_, please replace _*XXX*_ by the name of the distribution. It could be *leap* or *tumbleweed*.
Becareful to run the correct script, if not it may damage your installation.

Keybinding guide:
====

##### Movement / Layout:
* _win + hjkl_: Move focus
* _win + shift + hjkl_: Move node
* _win + 1234567_: change worskspace
* _win + shift + 1234567_: send node to a workspace
* _win + [_: Previous relative workspace
* _win + ]_: Next relative workspace
* _win + m_: Switch monocle mode (aka fullscreen)
* _win + tab_: Switch to next node
* _win + s_: set a window to floating mode
* _win + t_: set a window to tiled mode
* _win + shift + q_: Kill the current window

##### Launcher / Status bar:
* _win + space_: Regular app launcher
* _win + shift + space_: List of open app
* _win + shift + d_: Unregular app launcher
* _win + grave_: Tool dialog, for quick access
* _win + shift + x_: Out dialog, for shutdown, lock, reboot...etc

##### Quick Open:
* _win + e_: Open file manager
* _win + enter_: Open terminal app

##### Misc:
* _win + minus_: Low sound, launch notification with dunst
* _win + plus_: Up sound, launch notification with dunst
* _win + 0_: Toggle muted sound, launch notification with dunst
* _win + shift + minus_: Low brightness, launch notification with dunst
* _win + shift + plus_: Up brightness, launch notification with dunst

##### Mouse:
* _win + left click_: move
* _win + right click_: resize 


Further Note:
====

* Almost every script use by bspwm key bindings, or effet are in _$HOME/.script_
There are all accessible directly in your shell because the directory is added
to the path.
	So if you need to change your wallpaper, please use this command:
```text
set_wallpaper /path/to/your/wallpaper
```

I invite you to give a look at this directory and the scripts that it contain.

* If your using this rice on a real PC please uncomment line number 31 and line
number 35 in your _$HOME/.config/compton/compton.conf_.
You may change the compton file depending on your graphical card

* Want to use some dropdown terminal ?
if you use Guake, just uncomment line 39 in this file _$HOME/.config/bspwm/bspwmrc_.
if you don't use Guake
Please copy the rule line 30 on _$HOME/.config/bspwm/bspwmrc_, change ```Guake``` by your dropdown terminal class name.
add the background exec of your dropdown terminal after ```sxhkd &```.

* I'm using Dunst for notification on the system
* Sakura Terminal is erasing conf at his first start, you may re-copy the config file from the repo in _$HOME/.config/sakura_ to make it work
