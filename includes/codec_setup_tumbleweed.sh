#!/bin/bash

echo "Installation of Codec will begin..."
read -s -p "Enter your password here for sudo: " sudoPw
echo -e "\n"

echo "Please becareful this script is not fully automatic"

echo $sudoPw | sudo -S zypper --non-interactive --quiet addrepo --refresh -p 90 http://packman.inode.at/suse/openSUSE_Tumbleweed/ 'packman'
echo $sudoPw | sudo -S zypper --gpg-auto-import-keys refresh
echo $sudoPw | sudo -S zypper --non-interactive dist-upgrade --allow-vendor-change --from packman
echo $sudoPw | sudo -S zypper --non-interactive dist-upgrade --allow-vendor-change --from dvd
echo $sudoPw | sudo -S zypper -n in --from 1 $(cat p_codec);
echo $sudoPw | sudo -S zypper  in $(cat p_codec);
