#!/bin/zsh

#=============================================================================#
#-----------------------------------------------------------------------------#
bright(){
	CHK=$(ls -1 /sys/class/backlight/ | wc -l)
	if [ $CHK -eq 0 ]
	then
		echo -en " none "
	else
		MAX_BR=$(cat /sys/class/backlight/intel_backlight/max_brightness)
		CR_BR=$(cat /sys/class/backlight/intel_backlight/actual_brightness)
		PER_BR=$(echo "100*$CR_BR/$MAX_BR" | bc)
		echo -en " $PER_BR%"
	fi
}

bright
