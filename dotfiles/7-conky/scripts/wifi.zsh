#!/bin/zsh

#=============================================================================#
#-----------------------------------------------------------------------------#
connect(){
	NET_STAT=$(ip addr show | awk '/inet.*brd/{print $NF; exit}' | cut -c 1-3);
	STR1=""
	STR2=""
	STR3=""
	STR4=""
	STR5=""
	if [ $NET_STAT = 'wlp' ];
	then
		echo -en "${STR1}${STR2}${STR3}${STR4}${STR5}";
	elif [ $NET_STAT != 'wlp' ];
	then
		echo -en "${STR1}${STR2}${STR3}${STR4}${STR5}";
	else
		STR1="";
		STR2="";
		STR3="";
		STR4="";
		STR5="";
		echo -en "${STR1}${STR2}${STR3}${STR4}${STR5}";
	fi
}

connect
