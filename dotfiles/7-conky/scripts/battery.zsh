#!/bin/zsh

#=============================================================================#
#-----------------------------------------------------------------------------#
battery(){
	PWR_STAT=$(grep POWER_SUPPLY_STATUS /sys/class/power_supply/BAT0/uevent | \
		cut -f2 -d"=");
	PWR_PR=$(grep POWER_SUPPLY_CAPACITY /sys/class/power_supply/BAT0/uevent | \
		cut -f2 -d"=" | head -n 1);
	echo -en " $PWR_PR% ";
}

battery
